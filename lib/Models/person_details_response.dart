import 'person_details.dart';

class PersonDetailsResponse{
  final List<PersonDetails> personDetails;
  final String error;
  final List<PersonDetails> personDetails2;

  PersonDetailsResponse(this.personDetails,this.error,this.personDetails2);

  PersonDetailsResponse.fromJson(Map<String , dynamic> json) :
        personDetails = (json["cast"] as List).map((e) => new PersonDetails.fromJson(e)).toList(),
        personDetails2 = (json["crew"] as List).map((e) => new PersonDetails.fromJson(e)).toList(),
        error = "";


  PersonDetailsResponse.withError(String errorValue) :
        personDetails = List(),
        personDetails2 = List(),
        error = errorValue;
}



