import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutterapp/Bloc/get_nowplaying_bloc.dart';
import 'package:flutterapp/Models/movie.dart';
import 'package:flutterapp/Models/movie_response.dart';
import 'package:page_indicator/page_indicator.dart';
import 'package:flutterapp/Style/theme.dart' as Style;
import 'package:font_awesome_flutter/font_awesome_flutter.dart';


class NowPlaying extends StatefulWidget {
  @override
  _NowPlayingState createState() => _NowPlayingState();
}

class _NowPlayingState extends State<NowPlaying> {

  @override
  void initState() {
    nowPlayingMoviesBloc.getMovies();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<MovieResponse>(
        stream: nowPlayingMoviesBloc.subject.stream,
        builder: (context , AsyncSnapshot<MovieResponse> snapshot){
          if(snapshot.hasData){
            if(snapshot.data.error != null && snapshot.data.error.length > 0){
              return _buildError(snapshot.data.error);
            }
            return _buildNowPlaying(snapshot.data);
          } else if(snapshot.hasError){
            return _buildError(snapshot.error);
          } else {
            return _buildLoading();
          }
        }
    );
  }

  Widget _buildLoading(){
    return Center(
      child: Padding(padding: EdgeInsets.only(top: 10),
        child: CircularProgressIndicator(

        ),),
    );
  }

  Widget _buildError(String error){
    return Center(
      child: Column(mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text('Error Occured: $error')
        ],
      ),
    );
  }

  Widget _buildNowPlaying(MovieResponse data){
    List<Movie> movies = data.movies;
    if(movies.length == 0){
      return Container(width: MediaQuery.of(context).size.width,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text('No movies')
          ],
        ),

      );
    } else {
      return Container(height: 220,
        child: PageIndicatorContainer(
            align: IndicatorAlign.bottom,
            indicatorSpace: 8,
            padding: EdgeInsets.all(5),
            indicatorColor: Style.Colors.titleColor,
            indicatorSelectorColor: Style.Colors.secondColor,
            shape: IndicatorShape.circle(size: 8),
            length:5,
            child: PageView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: movies.take(5).length,
              itemBuilder: (context , index){
                return Stack(
                  children: <Widget>[
                    Container(width: MediaQuery.of(context).size.width,height: 220,
                      decoration: BoxDecoration(
                          shape: BoxShape.rectangle,
                          image: DecorationImage(image: NetworkImage("https://image.tmdb.org/t/p/w500" + movies[index].backPoster),
                              fit: BoxFit.cover)
                      ),
                    ),
                    Container(
                      decoration: BoxDecoration(
                          gradient: LinearGradient(colors: [
                            Style.Colors.mainColor.withOpacity(1.0),
                            Style.Colors.mainColor.withOpacity(0.0)
                          ],
                              begin: Alignment.bottomCenter,
                              end: Alignment.topCenter,
                              stops: [0,0.9]

                          )
                      ),
                    ),


                    Positioned(
                      bottom: 30,
                      child: Padding(padding: const EdgeInsets.only(left: 10),
                        child: Text(movies[index].title,style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16),),
                      ),
                    )
                  ],
                );
              },
            )




        ),);
    }

  }


}
