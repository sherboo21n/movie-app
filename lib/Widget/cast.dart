import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutterapp/Bloc/get_cast_bloc.dart';
import 'package:flutterapp/Models/cast.dart';
import 'package:flutterapp/Models/cast_response.dart';
import 'package:flutterapp/Models/person.dart';
import 'package:flutterapp/Models/person_details.dart';
import 'package:flutterapp/Screans/person_details.dart';
import 'package:flutterapp/Style/theme.dart' as Style;

class MoviesCast extends StatefulWidget {

  final int id;
  MoviesCast({Key key , @required this.id});

  @override
  _MoviesCastState createState() => _MoviesCastState(id);
}

class _MoviesCastState extends State<MoviesCast> {

  final int id;
  _MoviesCastState(this.id);

  static const baseImagesUrl = "https://image.tmdb.org/t/p/";

  @override
  void initState() {
    castBloc..getCasts(id);
    super.initState();
  }

  @override
  void dispose() {
    castBloc..drainStream();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(padding: EdgeInsets.only(left: 10,top: 20),
          child: Text('CAST' , style: TextStyle(color: Style.Colors.titleColor,fontWeight: FontWeight.bold,fontSize: 12),),
        ),
        SizedBox(height: 5,),

        StreamBuilder<CastResponse>(
            stream: castBloc.subject.stream,
            builder: (context, AsyncSnapshot<CastResponse> snapshot) {
              if (snapshot.hasData) {
                if (snapshot.data.error != null && snapshot.data.error.length > 0) {
                  return _buildError(snapshot.data.error);
                }
                return _buildMovieCast(snapshot.data);
              } else if (snapshot.hasError) {
                return _buildError(snapshot.error);
              } else {
                return _buildLoading();
              }
            }
        )



      ],
    );
  }

  Widget _buildLoading(){
    return Center(
      child: Padding(padding: EdgeInsets.only(top: 10),
        child: CircularProgressIndicator(
        ),),
    );
  }

  Widget _buildError(String error) {
    return Center(
      child: Column(mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text('Error Occured: $error')
        ],
      ),
    );
  }

  Widget _buildMovieCast(CastResponse data){
    List<Cast> casts = data.casts;
    return Container(
      height: 130,
      padding: EdgeInsets.only(left: 10),
      child:  ListView(scrollDirection: Axis.horizontal,
          children: casts == null ? <Widget>[
            Center(child: Padding(padding: const EdgeInsets.only(left: 20),
              child: CircularProgressIndicator(),
            ),)] :
          casts.map((e) => Padding(
            padding: EdgeInsets.only(left: 4),
            child: Container(width: 70,
              child: Column(children: <Widget>[
                GestureDetector(
                  onTap: (){
//                    Navigator.push(context, MaterialPageRoute(builder: (context) =>PersonDetailsScrean(
//                movie: casts2
//                    )));
                  },
                  child: CircleAvatar(radius: 28,backgroundImage: e.image != null ? NetworkImage("${baseImagesUrl}w154${e.image}") :
                  AssetImage("images/person.png")),
                ),
                Padding(padding: EdgeInsets.only(top: 4,left: 4),
                  child: Text(e.name ,maxLines: 2, style: TextStyle(
                      fontSize: 9,fontWeight: FontWeight.bold,color: Colors.white),),),
                SizedBox(height: 5,),
                Text(e.character , style: TextStyle(fontSize: 8,color: Style.Colors.titleColor),),
              ],),),
          )).toList()
      ),
    );

  }
}
