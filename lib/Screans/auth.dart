import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutterapp/Provider/auth_provider.dart';
import 'package:flutterapp/Style/theme.dart' as Style;
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:flutterapp/Animation/fade_animation.dart';


enum AuthMode { Signup , Login}

class AuthScrean extends StatefulWidget {
  @override
  _AuthScreanState createState() => _AuthScreanState();
}

class _AuthScreanState extends State<AuthScrean> {


  @override
  Widget build(BuildContext context) {

    final _deviceSize = MediaQuery.of(context).size;

    return Scaffold(
      body: Builder(builder: (context) =>Stack(
        children: <Widget>[
          Container(decoration: BoxDecoration(
            image: DecorationImage(image: AssetImage("images/movies.png"),fit: BoxFit.cover),),
            child: Container(decoration: BoxDecoration(
                color: Colors.black.withOpacity(0.5)
            ),),
          ),
          Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.bottomCenter,
                    end: Alignment.topCenter,
                    colors: [
                      Colors.black.withOpacity(0.90),
                      Colors.black38.withOpacity(0.5),
                    ])
            ),
          ),

          SingleChildScrollView(
            child: Container(height: _deviceSize.height,width: _deviceSize.width,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Flexible(
                   child: Padding(
                     padding: const EdgeInsets.only(bottom:25),
                     child: Row(mainAxisAlignment: MainAxisAlignment.center,
                       children: <Widget>[
                        FadeAnimation(3,Text('WatCh ',style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontSize: 48),
                        textAlign: TextAlign.center,),),
                      FadeAnimation(4,Icon(Icons.priority_high,color: Style.Colors.secondColor,size: 48,))
                       ],),
                   )
                  ),

                  Flexible(
                    flex: _deviceSize.width > 600? 2 : 1,
                    child: AuthCard()
                  )
                ],
              ),
            ),
          ),

        ],),),

    );
  }
}

class AuthCard extends StatefulWidget {
  AuthCard({Key key}) : super(key: key);
  @override
  _AuthCardState createState() => _AuthCardState();
}

class _AuthCardState extends State<AuthCard> {
  final GlobalKey<FormState> _formKey = GlobalKey();
  AuthMode _authMode = AuthMode.Login;
  Map<String , String > _authData = {
    "email": "",
    "password": ""
  };

  bool _isLoading = false;
  final _passwordController = TextEditingController();
  final _focusPasswowdNode = FocusNode();
  final _focusConfirmPasswowdNode = FocusNode();



  @override
  void dispose() {
    _focusPasswowdNode.dispose();
    final _focusConfirmPasswowdNode = FocusNode();
    super.dispose();
  }

  void showeDieloge(String message){
    showDialog(context:context ,barrierDismissible: false,
      builder: (context) =>  Container(width: MediaQuery.of(context).size.width,height: MediaQuery.of(context).size.height,
        child: Padding(padding: const EdgeInsets.only(top: 230,bottom: 270,left: 50,right: 50),
          child: Card( color: Colors.grey.shade900,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20)),
              child: Container(height: 260,
                child:  Column(children: <Widget>[
                  Padding(padding: const EdgeInsets.only(top: 30),
                    child: Text("An Error Occured!",textAlign: TextAlign.center,
                      style: TextStyle(color: Theme.of(context).accentColor,fontWeight: FontWeight.bold),),),
                  Padding(padding: const EdgeInsets.only(top: 8,left: 16,right: 16),
                    child: Text(message,textAlign: TextAlign.center,
                        style: TextStyle(color: Theme.of(context).accentColor,fontSize: 14)),),
                  Padding(padding: const EdgeInsets.only(top: 16),
                    child: Divider(color: Colors.grey,),),
                  FlatButton(child: Text("Okay",style: TextStyle(color: Colors.blue.shade800,),),
                      onPressed: (){Navigator.of(context).pop();}),
                ],),
              )),),
      ),
    );
  }

  Future<void> sumbit() async{
    final _formData = _formKey.currentState;
    if(_formData.validate()){}
    _formKey.currentState.save();
    setState(() {
      _isLoading = true;
    });
    try{
      if(_authMode == AuthMode.Login){
        await Provider.of<Auth>(context , listen: false).login(_authData["email"], _authData["password"]);
      }else {
        await  Provider.of<Auth>(context , listen: false).signup(_authData["email"], _authData["password"]);
      }

    } on HttpException catch(error){
      var errorMessage = "Authentication failed!";
      if(errorMessage.toString().contains("EMAIL_EXISTS")){
        errorMessage = "The e-mail address is already use";
      } else if(error.toString().contains("INVALID_EMAIL")){
        errorMessage = "This is not a vaild e-mail address";
      } else if(error.toString().contains("WEAK_PASSWORD")){
        errorMessage = "The password is too weak";
      } else if(error.toString().contains("EMAIL_NOT_FOUND")){
        errorMessage = "Coudnt found a user with this e-mail";
      } else if(error.toString().contains("INVALID_PASSWORD")){
        errorMessage = "Invaild Passwrd";
      }
      showeDieloge(errorMessage);
    } catch(eroor){
      const errorMessage = "Coudnt authenticate you can try again later";
      showeDieloge(errorMessage);
    }
    setState(() {
      _isLoading  =false;
    });
  }


  void switchAuthMode() {
    if(_authMode == AuthMode.Login){
      setState(() {
        _authMode = AuthMode.Signup;
      });
    }else {
      setState(() {
        _authMode = AuthMode.Login;
      });
    }
  }


  @override
  Widget build(BuildContext context) {
    final _deviceSize = MediaQuery.of(context).size;
    return  FadeAnimation(5,Card(
      elevation: 8,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20)),
      child: Container(height: _authMode == AuthMode.Signup ? 350 : 300,
        width: _deviceSize.width * 0.75,
        padding: EdgeInsets.all(16),
        child: Form(
          key: _formKey,
          child: SingleChildScrollView(
            child: Column(children: <Widget>[
              FadeAnimation(5.1, TextFormField(
                onFieldSubmitted: (_){
                  FocusScope.of(context).requestFocus(_focusPasswowdNode);},
                decoration: InputDecoration(labelText: "E-Mail:"),
                keyboardType: TextInputType.emailAddress,
                validator: (value){
                  if(value.isEmpty || !value.contains("@")){
                    return "Invaild Email";
                  }return null;
                },
                onSaved: (value){
                  _authData["email"] = value;
                },
              ),),
              FadeAnimation(5.2, TextFormField(
                focusNode: _focusPasswowdNode,
                onFieldSubmitted: (_){
                  FocusScope.of(context).requestFocus(_focusConfirmPasswowdNode);},
                controller: _passwordController,
                obscureText: true,
                decoration: InputDecoration(labelText: "Password:",),
                validator: (value){
                  if(value.isEmpty || value.length < 5){
                    return "Your Password Must Be > 5";
                  }return null;
                },
                onSaved: (value){
                  _authData["password"] = value;
                },
              )),
              if(_authMode == AuthMode.Signup)
             FadeAnimation(1, TextFormField(
                  focusNode: _focusConfirmPasswowdNode,
                  obscureText: true,
                  enabled: _authMode == AuthMode.Signup,
                  decoration: InputDecoration(labelText: "Confirm Password:",),
                  validator: _authMode == AuthMode.Signup ? (value){
                    if(value != _passwordController.text){
                      return "Password Dont Match!";
                    }return null;
                  } : null ,
                ),),

              SizedBox(height: 20,),
              if(_isLoading)
                CircularProgressIndicator()
              else FadeAnimation(6,RaisedButton(child: Text(_authMode == AuthMode.Login ? "LOGIN" : "SIGN UP",
                style: TextStyle(color: Colors.black,),),
                  onPressed: sumbit,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30)),
                  padding: EdgeInsets.only(top: 10,bottom: 10,right: 35,left: 35),
                  color: Style.Colors.secondColor
              ),),
              Padding(
                padding: const EdgeInsets.only(left: 20,right: 20),
                child: FadeAnimation(7,RaisedButton(onPressed: (){},
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
                  color: Colors.white,child: Row(mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[Icon(FontAwesomeIcons.google,color:Colors.pink,),
                      SizedBox(width: 5.0,),
                      Text("Login with Google",style: TextStyle(color: Colors.black,fontSize: 18.0),)],),
                ),)
              ),
              FadeAnimation(8, FlatButton(child: Text("${_authMode == AuthMode.Login ? "SIGNUP" : "LOGIN"} INSTEAD",
                  style: TextStyle( color: Style.Colors.secondColor)),
                onPressed: switchAuthMode,
              ))
            ],),
          ),
        ),
      ),
    ));


  }
}
