import 'package:flutterapp/Models/cast_response.dart';
import 'package:flutterapp/Models/genre_response.dart';
import 'package:flutterapp/Models/movie_details_response.dart';
import 'package:flutterapp/Models/movie_response.dart';
import 'package:flutterapp/Models/person_details_response.dart';
import 'package:flutterapp/Models/person_response.dart';
import 'package:flutterapp/Models/video_response.dart';
import 'package:dio/dio.dart';

class MovieRepository {
  static const baseUrl = "https://api.themoviedb.org/3/movie/";
  static const baseImagesUrl = "https://image.tmdb.org/t/p/";
  static const apiKey = "7b09cf0705817b4ab13f653c7d0c7b99";
  final Dio _dio = Dio();
  static const getGenreUrl = "https://api.themoviedb.org/3/genre/movie/list";
  static const getPersonUrl =  "https://api.themoviedb.org/3/trending/person/week";
  static const getPersonDetails = "https://api.themoviedb.org/3/person/";






  Future<PersonDetailsResponse> getPersonDetail(int id) async{
    var params = {
      "api_key": apiKey,
      "language": "en_US",
    };
    try{
      Response response = await _dio.get(getPersonDetails + "$id"  + "/movie_credits", queryParameters: params);
      return PersonDetailsResponse.fromJson(response.data);
    } catch (error , stacktarace){
      print("Exception occures: $error stackTracer: $stacktarace");
      return PersonDetailsResponse.withError("$error");
    }
  }


  Future<MovieResponse> getMovies() async{
    var params = {
      "api_key": apiKey,
      "language": "en_US",
      "page:": 1,
    };
    try{
      Response response = await _dio.get(baseUrl + "now_playing" , queryParameters: params);
      return MovieResponse.fromJson(response.data);
    }catch(error , stacktracer){
      print("Exception occures: $error stackTracer: $stacktracer");
      return MovieResponse.withError("$error");
    }
  }

  Future<MovieResponse> getPlayingMovie() async{
    var params = {
      "api_key": apiKey,
      "language": "en_US",
      "page:": 1,
    };
    try{
      Response response = await _dio.get(baseUrl + "now_playing" , queryParameters: params);
      return MovieResponse.fromJson(response.data);
    }catch(error , stacktracer){
      print("Exception occures: $error stackTracer: $stacktracer");
      return MovieResponse.withError("$error");
    }
  }

  Future<MovieResponse> getTopRatedMovie() async{
    var params = {
      "api_key": apiKey,
      "language": "en_US",
      "page:": 1,
    };
    try{
      Response response = await _dio.get(baseUrl + "top_rated" , queryParameters: params);
      return MovieResponse.fromJson(response.data);
    }catch(error , stacktracer){
      print("Exception occures: $error stackTracer: $stacktracer");
      return MovieResponse.withError("$error");
    }
  }

  Future<GenreResponse> getGenres() async{
    var params = {
      "api_key": apiKey,
      "language": "en_US",
      "page:": 1,
    };
    try{
      Response response = await _dio.get(getGenreUrl, queryParameters: params);
      return GenreResponse.fromJson(response.data);
    }catch(error , stacktracer){
      print("Exception occures: $error stackTracer: $stacktracer");
      return GenreResponse.withError("$error");
    }
  }

  Future<PersonResponse> getPerson() async{
    var params = {
      "api_key": apiKey,
    };
    try{
      Response response = await _dio.get(getPersonUrl, queryParameters: params);
      return PersonResponse.fromJson(response.data);
    }catch(error , stacktracer){
      print("Exception occures: $error stackTracer: $stacktracer");
      return PersonResponse.withError("$error");
    }
  }

  Future<MovieResponse> getMovieByGenre(int id) async{
    var params = {
      "api_key": apiKey,
      "language": "en_US",
      "page:": 1,
      "with_genres": id,
    };
    try{
      Response response = await _dio.get(baseUrl + "popular" , queryParameters: params);
      return MovieResponse.fromJson(response.data);
    }catch(error , stacktracer){
      print("Exception occures: $error stackTracer: $stacktracer");
      return MovieResponse.withError("$error");
    }
  }

  Future<MovieDetailsResponse> getMovieDetail(int id) async{
    var params = {
      "api_key": apiKey,
      "language": "en_US",
    };
    try{
      Response response = await _dio.get(baseUrl + "$id" , queryParameters: params);
      return MovieDetailsResponse.fromJson(response.data);
    } catch (error , stacktarace){
      print("Exception occures: $error stackTracer: $stacktarace");
      return MovieDetailsResponse.withError("$error");
    }
  }

  Future<CastResponse> getCasts(int id) async{
    var params = {
      "api_key": apiKey,
      "language": "en_US",
    };
    try{
      Response response = await _dio.get(baseUrl + "$id" + "/credits", queryParameters: params);
      return CastResponse.fromJson(response.data);
    } catch (error , stacktarace){
      print("Exception occures: $error stackTracer: $stacktarace");
      return CastResponse.withError("$error");
    }
  }

  Future<MovieResponse> getSimilarMovies(int id) async{
    var params = {
      "api_key": apiKey,
      "language": "en_US",
    };
    try{
      Response response = await _dio.get(baseUrl + "$id" + "/similar", queryParameters: params);
      return MovieResponse.fromJson(response.data);
    } catch (error , stacktarace){
      print("Exception occures: $error stackTracer: $stacktarace");
      return MovieResponse.withError("$error");
    }
  }

  Future<VideoResponse> getMovieVideos(int id) async{

    var params = {
      "api_key": apiKey,
      "language": "en-US"
    };
    try{
      Response response = await _dio.get(baseUrl + "$id" + "/videos" , queryParameters: params);
      return VideoResponse.fromJson(response.data);

    }catch(error , stacktracer){
      print("Exception occures: $error stackTracer: $stacktracer");
      return VideoResponse.withError("$error");
    }
  }


}