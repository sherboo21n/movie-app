
import 'package:flutterapp/Models/person_response.dart';
import 'package:flutterapp/Repository/repository.dart';
import 'package:rxdart/subjects.dart';

class PersonListBloc{
  final MovieRepository _repository = MovieRepository();
  final BehaviorSubject<PersonResponse> _subject = BehaviorSubject<PersonResponse>();

  getPerson() async{
    PersonResponse response = await _repository.getPerson();
    _subject.sink.add(response);
  }

  dispose(){
    _subject.close();
  }

  BehaviorSubject<PersonResponse> get subject =>_subject;

}

final PersonBloc = PersonListBloc();