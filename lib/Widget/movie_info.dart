import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutterapp/Bloc/get_movie_details.dart';
import 'package:flutterapp/Models/movie_details.dart';
import 'package:flutterapp/Models/movie_details_response.dart';
import 'package:flutterapp/Style/theme.dart' as Style;
import 'cast.dart';



class MovieInfo extends StatefulWidget {

  final int id;
  MovieInfo({Key key, @required this.id}) : super(key:key);

  @override
  _MovieInfoState createState() => _MovieInfoState(id);
}

class _MovieInfoState extends State<MovieInfo> {

  final int id;
  _MovieInfoState(this.id);


  @override
  void initState() {
    movieDetailsBloc..getMovieDetails(id);
    super.initState();
  }

  @override
  void dispose() {
    movieDetailsBloc..drainStream();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<MovieDetailsResponse>(
        stream: movieDetailsBloc.subject.stream,
        builder: (context, AsyncSnapshot<MovieDetailsResponse> snapshot) {
          if (snapshot.hasData) {
            if (snapshot.data.error != null && snapshot.data.error.length > 0) {
              return _buildError(snapshot.data.error);
            }
            return _buildMovieInfo(snapshot.data);
          } else if (snapshot.hasError) {
            return _buildError(snapshot.error);
          } else {
            return Container();
          }
        }
    );
  }


  Widget _buildError(String error) {
    return Center(
      child: Column(mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text('Error Occured: $error')
        ],
      ),
    );
  }

  Widget  _buildMovieInfo(MovieDetailsResponse data){
    MovieDetails details = data.movieDetails;
    return Column(crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Padding(padding: EdgeInsets.only(left: 10,right: 10),
            child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Column(children: <Widget>[
                  Text('BUDGET' , style: TextStyle(color: Style.Colors.titleColor,fontSize: 10
                  ),),
                  SizedBox(height: 5,),
                  Text(
                    details.budget.toString() + "\$",style: TextStyle(color: Style.Colors.secondColor,fontSize: 12,fontWeight: FontWeight.bold),
                  ),
                ],),
                Column(children: <Widget>[
                  Text('DURATION' , style: TextStyle(color: Style.Colors.titleColor,fontSize: 10
                  ),),
                  SizedBox(height: 5,),
                  Text(
                    details.runtime.toString() + "min",style: TextStyle(color: Style.Colors.secondColor,fontSize: 12,fontWeight: FontWeight.bold),
                  ),
                ],),
                Column(children: <Widget>[
                  Text('RELASE DATA' , style: TextStyle(color: Style.Colors.titleColor,fontSize: 10
                  ),),
                  SizedBox(height: 5,),
                  Text(details.releaseData,
                    style: TextStyle(color: Style.Colors.secondColor,fontSize: 12,fontWeight: FontWeight.bold),)

                ],)
              ],)
        ),
        Padding(
          padding: const EdgeInsets.only(left: 8),
          child: Container(height:MediaQuery.of(context).size.height/10,width:MediaQuery.of(context).size.width ,
            child: ListView(scrollDirection: Axis.horizontal,
                children:  details == null ? []  : details.genres.map((e) => Padding(
                  padding: EdgeInsets.only(right: 6),
                  child: FilterChip(backgroundColor: Colors.grey.shade600,labelStyle: TextStyle(fontSize: 10,fontWeight: FontWeight.bold),
                      label:  Text(e.name) ,onSelected: (b){}),
                )).toList()
            ),
          ),),
        MoviesCast(id: details.id,)
      ],
    );
  }
}
