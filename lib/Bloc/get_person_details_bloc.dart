import 'package:flutter/material.dart';
import 'package:flutterapp/Models/movie_details_response.dart';
import 'package:flutterapp/Models/person_details_response.dart';
import 'package:flutterapp/Repository/repository.dart';
import 'package:rxdart/subjects.dart';

class PersonDetailsBloc{
  final MovieRepository _repository = MovieRepository();
  final BehaviorSubject<PersonDetailsResponse> _subject = BehaviorSubject<PersonDetailsResponse>();

  getPersonDetails(int id) async{
    PersonDetailsResponse response = await _repository.getPersonDetail(id);
    _subject.sink.add(response);
  }


  void drainStream() {
    _subject.value = null;
  }
  @mustCallSuper

  void dispose() async{
    await _subject.drain();
    _subject.close();
  }

  BehaviorSubject<PersonDetailsResponse> get subject =>_subject;

}

final personDetailsBloc = PersonDetailsBloc();