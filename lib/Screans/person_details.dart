import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutterapp/Bloc/get_person_details_bloc.dart';
import 'package:flutterapp/Models/cast.dart';
import 'package:flutterapp/Models/person.dart';
import 'package:flutterapp/Models/person_details.dart';
import 'package:flutterapp/Models/person_details_response.dart';
import 'package:flutterapp/Style/theme.dart' as Style;

import 'movie_details.dart';



class PersonDetailsScrean extends StatefulWidget {

  final Person movie;
  PersonDetailsScrean({Key key , @required this.movie,}) :super(key: key);

  @override
  _PersonDetailsScreanState createState() => _PersonDetailsScreanState(movie,);
}

class _PersonDetailsScreanState extends State<PersonDetailsScrean> {

  final Person movie;
  _PersonDetailsScreanState(this.movie ,);

  static const baseImagesUrl = "https://image.tmdb.org/t/p/";

  @override
  void initState() {
    personDetailsBloc..getPersonDetails(movie.id);
    super.initState();
  }

  @override
  void dispose() {
    personDetailsBloc..drainStream();
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Style.Colors.mainColor,
      appBar: AppBar(title: Row(mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Text('WatCh ', style: TextStyle(color: Colors.white),),
          Icon(Icons.priority_high,color: Style.Colors.secondColor,)
        ],),
        backgroundColor: Style.Colors.mainColor,iconTheme: IconThemeData(color: Colors.white),
        leading: Padding(padding: const EdgeInsets.only(top: 8,left: 4),
          child: IconButton(icon: Icon(Icons.arrow_back),onPressed: (){
            Navigator.pop(context);
          },alignment: Alignment.topLeft),
        ),
        actions: <Widget>[IconButton(icon: Icon(EvaIcons.searchOutline),onPressed: (){},)],
      ),
      body: StreamBuilder<PersonDetailsResponse>(
          stream: personDetailsBloc.subject.stream,
          builder: (context, AsyncSnapshot<PersonDetailsResponse> snapshot) {
            if (snapshot.hasData) {
              if (snapshot.data.error != null && snapshot.data.error.length > 0) {
                return _buildError(snapshot.data.error);
              }
              return _buildPersonDetails(snapshot.data);
            } else if (snapshot.hasError) {
              return _buildError(snapshot.error);
            } else {
              return _buildLoading();
            }
          }
      ),
    );
  }

  Widget _buildLoading(){
    return Center(
      child: Padding(padding: EdgeInsets.only(top: 10),
        child: CircularProgressIndicator(
        ),),
    );
  }

  Widget _buildError(String error) {
    return Center(
      child: Column(mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text('Error Occured: $error')
        ],
      ),
    );
  }

  Widget _buildPersonDetails(PersonDetailsResponse data){
    List<PersonDetails> personDetails = data.personDetails;
     return   Container(
        child: ListView(children: <Widget>[
           Padding(padding: const EdgeInsets.only(top: 10,left: 10),
             child: Row(children: <Widget>[
                  CircleAvatar(radius: 30,backgroundImage: widget.movie.profileImage != null ?
                  NetworkImage("${baseImagesUrl}w154" + widget.movie.profileImage) :
                  AssetImage("images/person.png")),
                  Padding(
                    padding: const EdgeInsets.only(left: 10),
                    child: Column(
                      children: <Widget>[
                        Text(widget.movie.name,style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,),),

                        Text('(Actor)',style: TextStyle(color: Style.Colors.titleColor),),
                      ],),),
                ],),
           ),
           Divider(),

           Padding(
             padding: const EdgeInsets.only(top: 15),
             child: Container(width: MediaQuery.of(context).size.width,height: MediaQuery.of(context).size.height,
               child: GridView.builder(itemCount: personDetails.length,
                 gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
                 itemBuilder: (context , index)=>  GestureDetector(
                   onTap: (){

                   },
                   child: Column(children: <Widget>[
                     personDetails[index].poster_path== null ? Container(
                       width: 130.0,height: 180.0,
                       decoration: BoxDecoration(color: Style.Colors.secondColor,
                           borderRadius: BorderRadius.all(Radius.circular(8),),
                           shape: BoxShape.rectangle
                       ),
                       child: Column(children: <Widget>[
                         Icon(EvaIcons.filmOutline,color: Colors.white,size: 50.0,)
                       ],),
                     ) : Container(
                       width: 120.0,height: 180.0,
                       decoration: BoxDecoration(
                           borderRadius: BorderRadius.all(Radius.circular(2.0),),
                           shape: BoxShape.rectangle,
                           image: DecorationImage(image: NetworkImage("https://image.tmdb.org/t/p/w200" + personDetails[index].poster_path),
                               fit: BoxFit.cover)
                       ),
                     ),

                    Padding(
                      padding: const EdgeInsets.only(left: 8,right: 8,top: 5),
                      child: Text(personDetails[index].title , textAlign: TextAlign.center,overflow: TextOverflow.ellipsis,
                             style: TextStyle(height: 1.4,color: Colors.white,fontWeight: FontWeight.bold,fontSize: 10.0)
                         ),
                    ),

                   ],),
                 )
                 ),),
           )

        ]),
      );


  }


}