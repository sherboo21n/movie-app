import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutterapp/Bloc/get_movie_by_genre.dart';
import 'package:flutterapp/Models/movie.dart';
import 'package:flutterapp/Models/movie_response.dart';
import 'package:flutterapp/Screans/movie_details.dart';
import 'package:intl/intl.dart';
import 'package:flutterapp/Style/theme.dart' as Style;
import 'package:eva_icons_flutter/eva_icons_flutter.dart';

class GenreMovies extends StatefulWidget {
  final int genreId;
  GenreMovies({Key key , @required this.genreId}) : super(key : key);
  @override
  _GenreMoviesState createState() => _GenreMoviesState(genreId);
}

class _GenreMoviesState extends State<GenreMovies> {
  final int genreId;
  _GenreMoviesState(this.genreId);

  @override
  void initState() {
    moviesByGenresBloc.getMoviesByGenre(genreId);
    super.initState();
  }
  @override
  void dispose() {
    moviesByGenresBloc..drainStream();
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {
    return StreamBuilder<MovieResponse>(
        stream: moviesByGenresBloc.subject.stream,
        builder: (context, AsyncSnapshot<MovieResponse> snapshot) {
          if (snapshot.hasData) {
            if (snapshot.data.error != null && snapshot.data.error.length > 0) {
              return _buildError(snapshot.data.error);
            }
            return _buildMoviesByGenre(snapshot.data);
          } else if (snapshot.hasError) {
            return _buildError(snapshot.error);
          } else {
            return _buildLoading();
          }
        }
    );
  }

  Widget _buildLoading(){
    return Center(
      child: Padding(padding: EdgeInsets.only(top: 10),
        child: CircularProgressIndicator(
        ),),
    );
  }

  Widget _buildError(String error) {
    return Center(
      child: Column(mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text('Error Occured: $error')
        ],
      ),
    );
  }

  Widget _buildMoviesByGenre(MovieResponse data){
    List<Movie> movies = data.movies;
    if(movies.length ==0){
      return Container(
        child: Text("No Movies"),
      );
    } else {
      return Container(height: 270.0,
        padding: EdgeInsets.only(left: 10.0),
        child: ListView.builder(scrollDirection: Axis.horizontal,
          itemCount: movies.length,
          itemBuilder: (context , index){
            return Padding(padding: EdgeInsets.only(top: 10.0,bottom: 10.0,right: 10.0),
              child: GestureDetector(
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context) =>MovieDetailsScrean(
                    movie: movies[index],
                  )));
                },
                child: Column(children: <Widget>[
                  movies[index].poster == null ? Container(
                    width: 120.0,height: 180.0,
                    decoration: BoxDecoration(color: Style.Colors.secondColor,
                        borderRadius: BorderRadius.all(Radius.circular(2.0),),
                        shape: BoxShape.rectangle
                    ),
                    child: Column(children: <Widget>[
                      Icon(EvaIcons.filmOutline,color: Colors.white,size: 50.0,)
                    ],),
                  ) : Container(
                    width: 120.0,height: 180.0,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(2.0),),
                        shape: BoxShape.rectangle,
                        image: DecorationImage(image: NetworkImage("https://image.tmdb.org/t/p/w200" + movies[index].poster),
                            fit: BoxFit.cover)
                    ),
                  ),
                  SizedBox(height: 10.0,),
                  Container(
                    width: 100.0,
                    child: Text(movies[index].title,maxLines: 2,
                        style: TextStyle(height: 1.4,color: Colors.white,fontWeight: FontWeight.bold,fontSize: 12.0)
                    ),
                  ),
                  Row(children: <Widget>[
                    Text(movies[index].rating.toString(),style: TextStyle(
                        color: Colors.white,fontSize: 10.0,fontWeight: FontWeight.bold
                    ),),
                    SizedBox(height: 5,),
                    RatingBar(
                      itemSize: 8.0,
                      initialRating: movies[index].rating/2,
                      maxRating: 1,
                      direction: Axis.horizontal,
                      allowHalfRating: true,
                      itemCount: 5,
                      itemPadding: EdgeInsets.symmetric(horizontal: 2.0),
                      itemBuilder: (context , _) =>Icon(
                        EvaIcons.star,color: Style.Colors.secondColor,
                      ),
                      onRatingUpdate: (rating){
                        print(rating);
                      },
                    )
                  ],),
                  Text(DateFormat("yyyy").format(DateTime.parse(movies[index].releaseData)),textAlign: TextAlign.left,
                    style: TextStyle(color: Colors.white,fontSize: 12,fontWeight: FontWeight.bold),),

                ],),
              ),
            );
          },
        ),
      );
    }
  }


}
