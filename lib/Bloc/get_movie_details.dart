import 'package:flutter/material.dart';
import 'package:flutterapp/Models/movie_details_response.dart';
import 'package:flutterapp/Repository/repository.dart';
import 'package:rxdart/subjects.dart';

class MovieDetailsBloc{
  final MovieRepository _repository = MovieRepository();
  final BehaviorSubject<MovieDetailsResponse> _subject = BehaviorSubject<MovieDetailsResponse>();

  getMovieDetails(int id) async{
    MovieDetailsResponse response = await _repository.getMovieDetail(id);
    _subject.sink.add(response);
  }


  void drainStream() {
    _subject.value = null;
  }
  @mustCallSuper

  void dispose() async{
    await _subject.drain();
    _subject.close();
  }

  BehaviorSubject<MovieDetailsResponse> get subject =>_subject;

}

final movieDetailsBloc = MovieDetailsBloc();