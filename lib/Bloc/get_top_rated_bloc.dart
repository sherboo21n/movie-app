
import 'package:flutterapp/Models/movie_response.dart';
import 'package:flutterapp/Repository/repository.dart';
import 'package:rxdart/subjects.dart';

class TopRatedMoviesListBloc{
  final MovieRepository _repository = MovieRepository();
  final BehaviorSubject<MovieResponse> _subject = BehaviorSubject<MovieResponse>();

  getMovies() async{
    MovieResponse response = await _repository.getTopRatedMovie();
    _subject.sink.add(response);
  }

  dispose(){
    _subject.close();
  }

  BehaviorSubject<MovieResponse> get subject =>_subject;

}

final topRatedMoviesBloc = TopRatedMoviesListBloc();