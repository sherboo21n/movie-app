import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart'as http;
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';


class HttpException implements Exception{
  final String message;
  HttpException(this.message);

  @override
  String toString(){
    return message;
  }
}

class Auth extends ChangeNotifier {
  String token;
  DateTime expiryData;
  String userId;
  Timer  authTimer;
  bool get isAuth{
    return Token != null;
  }
  String get Token{
    if(expiryData != null && expiryData.isAfter(DateTime.now()) && token != null){
      return token;
    }
    return null;
  }

  Auth({this.token, this.expiryData, this.userId , this.authTimer});

  Future<void> _authenticate(String email, String password, String urlSegment) async {
    final Url = 'https://identitytoolkit.googleapis.com/v1/accounts:$urlSegment?key=AIzaSyCCkRLp4BPN-Imgam-SSbOCWG-TFROdXOE';

    try{
      final response = await http.post(Url, body: json.encode({
        "email": email,
        "password": password,
        "returnSecureToken": true,
      }));
      final responseData = json.decode(response.body);
      if(responseData["error"] != null){
        throw HttpException(responseData["error"]["message"]);
      }
      token = responseData["idToken"];
      userId = responseData["localId"];
      expiryData = DateTime.now().add(Duration(seconds: int.parse(responseData["expiresIn"])));
      autoLogOut();
      notifyListeners();

      WidgetsFlutterBinding.ensureInitialized();
      final prefs = await SharedPreferences.getInstance();
      final userData = json.encode({
        "token": token ,
        "userId": userId ,
        "expiryData": expiryData.toIso8601String()
      });
      prefs.setString("userData" , userData);

    } catch(error){
      print(error);
      throw (error);
    }
  }
  Future<void> login(String email, String password) async {
    return _authenticate(email, password, "signInWithPassword");
  }

  Future<void> signup(String email, String password) async {
    return _authenticate(email, password, "signUp");
  }

  Future<bool> tryAutoLogin() async{
    WidgetsFlutterBinding.ensureInitialized();
    final prefs = await SharedPreferences.getInstance();
    if(!prefs.containsKey("userData")){
      return false;
    }
    final extractUserData = json.decode(prefs.getString("userData")) as Map<String , dynamic>;
    final ExpiryData =  DateTime.parse(extractUserData["expiryData"]);

    if(ExpiryData.isBefore(DateTime.now())){
      return false;
    }
    token = extractUserData["token"];
    userId = extractUserData["userId"];
    expiryData = ExpiryData;
    notifyListeners();
    autoLogOut();
    return true;
  }

  Future<void> logOut() async {
    token = null;
    userId = null;
    expiryData = null;
    if(authTimer !=null){
      authTimer .cancel();
      authTimer = null;
    }
    notifyListeners();
    WidgetsFlutterBinding.ensureInitialized();
    final prefs = await SharedPreferences.getInstance();
    prefs.clear();
  }

  void autoLogOut(){
    if(authTimer !=null){
      authTimer .cancel();
    }
    final timeToExpire = expiryData.difference(DateTime.now()).inSeconds;
    authTimer = Timer(Duration(seconds: timeToExpire) , logOut);
  }
}
