import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutterapp/Models/movie.dart';
import 'package:flutterapp/Models/video_response.dart';
import 'package:flutterapp/Models/video.dart';
import 'package:flutterapp/Style/theme.dart' as Style;
import 'package:flutterapp/Widget/movie_info.dart';
import 'package:flutterapp/Widget/simillar_movies.dart';
import 'package:sliver_fab/sliver_fab.dart';
import 'package:flutterapp/Bloc/get_movie_videos_bloc.dart';


class MovieDetailsScrean extends StatefulWidget {

  final Movie movie;
  MovieDetailsScrean({Key key , @required this.movie}) :super(key: key);

  @override
  _MovieDetailsScreanState createState() => _MovieDetailsScreanState(movie);
}

class _MovieDetailsScreanState extends State<MovieDetailsScrean> {

  final Movie movie;
  _MovieDetailsScreanState(this.movie);

  static const baseImagesUrl = "https://image.tmdb.org/t/p/";

  @override
  void initState() {
    moviesVideoBloc..getMovieVideos(movie.id);
    super.initState();
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Style.Colors.mainColor,
        body:  SliverFab(
          floatingPosition: FloatingPosition(right: 20),
          floatingWidget: StreamBuilder<VideoResponse> (
            stream: moviesVideoBloc.subject.stream,
            builder: (context , AsyncSnapshot<VideoResponse> snapshot){
              if(snapshot.hasData){
                if(snapshot.data.error != null && snapshot.data.error.length > 0){
                  return _buildError(snapshot.data.error);
                }
                return _buildMovieVideo(snapshot.data);
              } else if(snapshot.hasError){
                return _buildError(snapshot.error);
              } else {
                return _buildLoading();
              }
            },
          ),

          expandedHeight: 250,
          slivers: <Widget>[
            SliverAppBar(backgroundColor: Style.Colors.mainColor,
              expandedHeight: 200,
              pinned: true,
              flexibleSpace: FlexibleSpaceBar(
                title: Text(widget.movie.title.length > 40 ? widget.movie.title.substring(0,37) + "..." :
                widget.movie.title,style: TextStyle(fontSize: 12,fontWeight: FontWeight.bold)),
                background: Stack(children: <Widget>[
                  Container(decoration: BoxDecoration(shape: BoxShape.rectangle,
                    image: DecorationImage(image: NetworkImage(
                        "${baseImagesUrl}w500" + widget.movie.backPoster
                    )),),
                    child: Container(decoration: BoxDecoration(
                        color: Colors.black.withOpacity(0.5)
                    ),),
                  ),
                  Container(
                    decoration: BoxDecoration(
                        gradient: LinearGradient(
                            begin: Alignment.bottomCenter,
                            end: Alignment.topCenter,
                            colors: [
                              Colors.black.withOpacity(0.5),
                              Colors.black.withOpacity(0.0),
                            ])
                    ),
                  )
                ],),
              ),
            ),
            SliverPadding(
                padding: EdgeInsets.all(0),
                sliver: SliverList(delegate: SliverChildListDelegate(
                    [
                      Padding(padding: EdgeInsets.only(left: 10,top: 20),
                        child: Row(crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Text(widget.movie.rating.toString(),
                              style: TextStyle(color: Colors.white,fontSize: 14,fontWeight: FontWeight.bold),),

                            RatingBar(
                              itemSize: 8.0,
                              initialRating: widget.movie.rating/2,
                              maxRating: 1,
                              direction: Axis.horizontal,
                              allowHalfRating: true,
                              itemCount: 5,
                              itemPadding: EdgeInsets.symmetric(horizontal: 2.0),
                              itemBuilder: (context , _) =>Icon(
                                EvaIcons.star,color: Style.Colors.secondColor,
                              ),
                              onRatingUpdate: (rating){
                                print(rating);
                              },
                            ),
                          ],),),

                      Padding(padding: EdgeInsets.only(left: 10,top: 20),
                        child: Text("OVERVIEW" , style: TextStyle(color: Style.Colors.titleColor,
                            fontWeight: FontWeight.bold,fontSize: 12),),),

                      SizedBox(height: 10),

                      Padding(padding: const EdgeInsets.only(left: 10,right: 10,top: 8),
                        child: Text(widget.movie.overView),),

                      Divider(),
                      MovieInfo(id: widget.movie.id,),
                      Divider(),
                      SimilarMovies(id: widget.movie.id,)
                    ]
                ),)
            )


          ],
        )
    );
  }

  Widget _buildLoading(){
    return Container();
  }

  Widget _buildError(String error){
    return Center(child: Column(children: <Widget>[
      Text(error)
    ],),);
  }

  Widget _buildMovieVideo(VideoResponse data){
    List <Video> videos = data.videos;
    return FloatingActionButton(
      backgroundColor: Style.Colors.secondColor,
      child: Icon(Icons.play_arrow),
      onPressed: (){


      },
    );

  }


}
