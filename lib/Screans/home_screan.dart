import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutterapp/Models/genre.dart';
import 'package:flutterapp/Screans/person_details.dart';
import 'package:flutterapp/Widget/genres.dart';
import 'package:flutterapp/Widget/now_playing.dart';
import 'package:flutterapp/Widget/person.dart';
import 'package:flutterapp/Widget/top_rated_movies.dart';
import 'package:flutterapp/Style/theme.dart' as Style;

class HomeScrean extends StatefulWidget {
  @override
  _HomeScreanState createState() => _HomeScreanState();
}

class _HomeScreanState extends State<HomeScrean> {


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Style.Colors.mainColor,
      appBar: AppBar(title: Row(mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text('WatCh ',style: TextStyle(color: Colors.white),),
          Icon(Icons.priority_high,color: Style.Colors.secondColor,)
        ],),centerTitle: true,
        backgroundColor: Style.Colors.mainColor,iconTheme: IconThemeData(color: Colors.white),
        leading: IconButton(icon: Icon(EvaIcons.menu2Outline),onPressed: (){},),
        actions: <Widget>[IconButton(icon: Icon(EvaIcons.searchOutline),onPressed: (){
          showSearch(context: context, delegate: DataSearch());
       },)],
      ),

      body: ListView(scrollDirection: Axis.vertical,
        children: <Widget>[
          NowPlaying(),
          GenreScrean(),
          Divider(),
          PersonList(),
          Divider(),
          TopMovies()

        ],
      ),
    );
  }
}

class DataSearch extends SearchDelegate<String>{


  final genres = [
   "Action",
    "action",
   "Adventure",
   "Animation",
   "Comedy",
   "Crime",
  "Documentary",
   "Drama",
  "Family",
   "Fantasy",
   "History",
   "Horror",
   "Music",
   "Mystery",
  "Romance",
  "Science Fiction",
  "TV Movie",
  "Thriller",
  "War",
  ];
  final recentCites = [
    "Adventure",
    "Animation",
    "Comedy",
    "Crime",
  ];

  @override
  ThemeData appBarTheme(BuildContext context) {
    return ThemeData.dark();

  }



  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(icon: Icon(Icons.clear),onPressed: (){
        query = "";

    },)];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(icon: AnimatedIcon(
      icon: AnimatedIcons.menu_arrow,
      progress: transitionAnimation,
    ),onPressed: (){
      close(context, null);

    },);
}

  @override
  Widget buildResults(BuildContext context) {
   return Container();
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    final suggestList = query.isEmpty ? recentCites : genres.
    where((e) => e.startsWith(query)).toList();
    return ListView.builder(itemCount: suggestList.length,
        itemBuilder: (context,index) =>ListTile(
          onTap: (){
            showResults(context);
          },
          leading: Icon(EvaIcons.refresh,color: Colors.grey.shade700),
          title: RichText(
            text: TextSpan(
                text: suggestList[index].substring(0, query.length),
                style: TextStyle(color: Colors.black , fontWeight: FontWeight.bold),
                children: [
                  TextSpan(text: suggestList[index].substring(query.length),
                      style: TextStyle(color: Style.Colors.titleColor))
                ]
            ),
          )
        ));
  }

}