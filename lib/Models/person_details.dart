

import 'dart:convert';

class PersonDetails{
  final int id;
  final String title;
  final String poster_path;
  final String releaseData;
  final String backdrop_path;

  PersonDetails({this.id,this.title,this.poster_path,this.releaseData,this.backdrop_path});

  PersonDetails.fromJson(Map<String , dynamic> json) :
        id = json["id"],
        title = json["title"],
        poster_path = json[" poster_path"],
        releaseData = json["release_date"],
  backdrop_path = json["backdrop_path"];

}
