import 'package:flutter/material.dart';
import 'package:flutterapp/Models/genre.dart';
import 'package:flutterapp/Style/theme.dart' as Style;
import 'genre_movie.dart';



class GenresList extends StatefulWidget {
  final List <Genre> genres;

  GenresList({Key key , @required this.genres}) : super(key: key);

  @override
  _GenresListState createState() => _GenresListState(genres);
}

class _GenresListState extends State<GenresList> with SingleTickerProviderStateMixin {

  final List<Genre> genres;
  TabController _tabController;
  _GenresListState(this.genres);

  @override
  void initState() {
    _tabController = TabController(vsync: this, length: genres.length);

    super.initState();
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 320,
      child: DefaultTabController(
        length: genres.length,
        child: Scaffold(
          backgroundColor: Style.Colors.mainColor,
          appBar: PreferredSize(
              child: AppBar(
                backgroundColor: Style.Colors.mainColor,
                bottom: TabBar(
                    controller: _tabController,
                    indicatorColor: Style.Colors.secondColor,
                    indicatorSize: TabBarIndicatorSize.tab,
                    indicatorWeight: 3.0,
                    unselectedLabelColor: Style.Colors.titleColor,
                    labelColor: Colors.white,
                    isScrollable: true,
                    tabs: genres.map((Genre genre){
                      return Container(
                        padding: EdgeInsets.only(bottom: 15,top: 10),
                        child: Text(genre.name.toUpperCase() , style: TextStyle(
                            fontSize: 14, fontWeight: FontWeight.bold
                        ),),
                      );
                    }).toList()

                ),
              ),

              preferredSize: Size.fromHeight(50.0)),
          body: TabBarView(
              controller: _tabController,
              physics: NeverScrollableScrollPhysics(),
              children: genres.map((Genre genre){
                return GenreMovies(genreId: genre.id,);
              }).toList()
          ),
        ),
      ),
    );
  }

}
