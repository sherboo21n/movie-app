import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutterapp/Bloc/get_person_bloc.dart';
import 'package:flutterapp/Models/person.dart';
import 'package:flutterapp/Models/person_details.dart';
import 'package:flutterapp/Models/person_response.dart';
import 'package:flutterapp/Screans/person_details.dart';
import 'package:flutterapp/Style/theme.dart' as Style;


class PersonList extends StatefulWidget {
  @override
  _PersonListState createState() => _PersonListState();
}

class _PersonListState extends State<PersonList> {

  @override
  void initState() {
    PersonBloc..getPerson();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(padding: EdgeInsets.only(left: 10),
          child: Text('TRENDING ACTORS ON THIS WEEK' , style: TextStyle(
              color: Style.Colors.titleColor,fontWeight: FontWeight.bold,fontSize: 12
          ),),
        ),
        SizedBox(height: 5,),
        StreamBuilder<PersonResponse>(
            stream: PersonBloc.subject.stream,
            builder: (context , AsyncSnapshot<PersonResponse> snapshot){
              if(snapshot.hasData){
                if(snapshot.data.error != null && snapshot.data.error.length > 0){
                  return _buildError(snapshot.data.error);
                }
                return _buildPersonList(snapshot.data);
              } else if(snapshot.hasError){
                return _buildError(snapshot.error);
              } else {
                return _buildLoading();
              }
            }
        )
      ],
    );

  }

  Widget _buildLoading(){
    return Center(
      child: Padding(padding: EdgeInsets.only(top: 10),
        child: CircularProgressIndicator(

        ),),
    );
  }

  Widget _buildError(String error){
    return Center(
      child: Column(mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text('Error Occured: $error')
        ],
      ),
    );
  }

  Widget _buildPersonList(PersonResponse data){
    List<Person> person = data.person;
    return Container(
      height: 120,
      padding: EdgeInsets.only(left: 10),
      child: ListView.builder(itemCount: person.length,scrollDirection: Axis.horizontal,
          itemBuilder: (context , index) {
            return Container(
              padding: EdgeInsets.only(top: 10,right: 8),
              child: GestureDetector(
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context) =>PersonDetailsScrean(
                      movie: person[index],
                  )));
                },
                child: Column(crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    person[index].profileImage == null ?
                    CircleAvatar(radius: 30,backgroundImage: AssetImage("images/person.png"),) :
                   CircleAvatar(radius: 30,backgroundImage: NetworkImage("https://image.tmdb.org/t/p/w200" + person[index].profileImage)),
                    SizedBox(height: 5,),

                    Text(person[index].name,style: TextStyle(color: Colors.white,fontSize: 10,fontWeight: FontWeight.bold),),
                    SizedBox(height: 5,),
                    Text('Trending for ${person[index].known}',
                      style: TextStyle(color: Style.Colors.titleColor,fontSize: 8),),
                  ],
                ),
              ),
            );
          }),

    );
  }
}

