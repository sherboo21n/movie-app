import 'package:flutter/material.dart';
import 'package:flutterapp/Screans/auth.dart';
import 'package:flutterapp/Style/theme.dart' as Style;
import './Screans/home_screan.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'Provider/auth_provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider.value(value: Auth()),
      ],
      child:
      Consumer<Auth>(builder: (context , auth , child) =>
          MaterialApp(
              theme: ThemeData.dark(),
              debugShowCheckedModeBanner: false,
              debugShowMaterialGrid: false,
              home: auth.isAuth ?  HomeScrean() : AuthScrean()

          ),
      ),
    );
  }
}
