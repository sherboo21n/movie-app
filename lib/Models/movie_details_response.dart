

import 'movie_details.dart';

class MovieDetailsResponse{
  final MovieDetails movieDetails;
  final String error;

  MovieDetailsResponse(this.movieDetails,this.error);

  MovieDetailsResponse.fromJson(Map<String , dynamic> json) :
        movieDetails = MovieDetails.fromJson(json),
        error = "";

  MovieDetailsResponse.withError(String errorValue) :
        movieDetails = MovieDetails(id: null, adult:null ,budget:null ,genres: null,releaseData:"" ,runtime:null ),
        error = errorValue;
}