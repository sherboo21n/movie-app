import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutterapp/Bloc/get_genre_bloc.dart';
import 'package:flutterapp/Models/genre.dart';
import 'package:flutterapp/Models/genre_response.dart';
import 'genres_list.dart';


class GenreScrean extends StatefulWidget {
  @override
  _GenreScreanState createState() => _GenreScreanState();
}

class _GenreScreanState extends State<GenreScrean> {


  @override
  void initState() {
    genresBloc..getGenres();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<GenreResponse>(
        stream: genresBloc.subject.stream,
        builder: (context, AsyncSnapshot<GenreResponse> snapshot) {
          if (snapshot.hasData) {
            if (snapshot.data.error != null && snapshot.data.error.length > 0) {
              return _buildError(snapshot.data.error);
            }
            return _buildGenre(snapshot.data);
          } else if (snapshot.hasError) {
            return _buildError(snapshot.error);
          } else {
            return Text("");
          }
        }
    );
  }


  Widget _buildError(String error) {
    return Center(
      child: Column(mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text('Error Occured: $error')
        ],
      ),
    );
  }

  Widget _buildGenre(GenreResponse data) {
    List<Genre> genres = data.genre;
    if (genres.length == 0) {
      return Container(
        child: Text("No Genres"),
      );
    } else
      return GenresList(genres: genres,);
  }
}